var app = angular.module("wedding", ['ngRoute']);


function getUrlParams() {
  var query = location.search.substr(1);
  var result = {};
  query.split("&").forEach(function(part) {
    var item = part.split("=");
    result[item[0]] = decodeURIComponent(item[1]);
  });
  return result;
}


app.controller("MainCtrl", function($scope, $routeParams, $http, $rootScope, $location) {
	
	$scope.info = true;
	$scope.bestmen = true;
	$scope.us = true;
	
	$scope.full = false;
	var params = getUrlParams();
	if(params.full){
		$scope.full = true;
	}
	
	
});

